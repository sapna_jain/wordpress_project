<?php
/*
 * Plugin Name: LearnDash - Exporter
 * Plugin URI: http://www.jumptoweb.com
 * Description: Generate a csv report of graduated students in a specific period of time.
 * Version: 1.0
 * Author: Manuel Costales
 * Author URI: http://www.mannycostales.com
*/

defined('ABSPATH') or die('Plugin file cannot be accessed directly.');
require_once 'learndash-settings-page.php';

function ld_admin_enqueue_scripts() {
    wp_enqueue_script('jquery');
    wp_enqueue_script('jquery-ui-core');
    wp_enqueue_script('jquery-ui-datepicker');

    wp_enqueue_script('ld-script', plugin_dir_url(__FILE__) . 'js/ld-exporter.js', array('jquery', 'jquery-ui-datepicker'), false, true);
    wp_enqueue_style('ld-style', plugin_dir_url(__FILE__) . 'css/ld-exporter.css');
}

add_action('admin_enqueue_scripts', 'ld_admin_enqueue_scripts');

if (is_admin()) $admin_page = new LearnDashExporterAdminPage();

function add_roles_on_plugin_activation() {
// Adding custom Role to Export Report
add_role('exporter', __(
    'Exporter'),
    array(
        'read' => true, // Allows a user to read
        'export' => true, //creating capability to export reports
    )
);
$role = get_role('exporter');
$role->add_cap('export');
// End of Adding custom Role to Export Report
}
register_activation_hook( __FILE__, 'add_roles_on_plugin_activation' );

function get_passed_quizzes($from, $to) {
    $date_format = 'Y-m-d H:i:s';
    $to->setTime(23, 59, 59);

    $all_users = get_users();
    $passedd_quizzes = array();
    foreach ($all_users as $user) {
        $quizzes = get_user_meta($user->ID, '_sfwd-quizzes', true);

        if (!empty($quizzes)) {
            foreach ($quizzes as $quiz) {
                $quiztime = $quiz['time'];

                if ($quiztime >= $from->getTimestamp() && $quiztime <= $to->getTimestamp() && $quiz['pass'] == 1) {
                    $course_id = learndash_get_course_id((int)$quiz['quiz']);
                    if (learndash_course_completed($user->ID, $course_id)) {
                        $course = get_post($course_id);
                        $userdata = get_userdata($user->ID);

                        $user_full_name = $userdata->display_name;
                        $user_email = $userdata->user_email;
                        $pass_time = new DateTime();
                        $pass_time->setTimestamp($quiz['time']);
                        $user_license_number = get_user_meta($user->ID, 'license', true);
                        $quiz_data = array(
                            'Name' => $user_full_name,
                            'User E-Mail' => $user_email,
                            'Pass Time' => $pass_time->format($date_format),
                            'Course Title' => get_the_title($course->ID),
                            'License ID' => $user_license_number
                        );
                        $passedd_quizzes[] = $quiz_data;
                    }
                }
            }
        }
    }
    return $passedd_quizzes;
}

function request_passed_exams() {
    if (!empty($_POST['action']) && $_POST['action'] == 'ld-generate-csv') {
        if (!empty($_POST['ld-date-from']) && !empty($_POST['ld-date-to'])) {

            $date_from = $_POST['ld-date-from'];
            $date_to = $_POST['ld-date-to'];

            $from = new DateTime();
            $to = new DateTime();
            $from->setTimestamp(strtotime($date_from));
            $to->setTimestamp(strtotime($date_to));
            $export_format = 'Y-m-d';

            $passed_quizzes = get_passed_quizzes($from, $to);
            init_file_download('learndash-'.$from->format($export_format).'-'.$to->format($export_format).".csv");
            echo array_to_csv($passed_quizzes);
            die();
            //$content = generate_csv($passed_quizzes);
            //init_file_download($content);
            //die();


        } else {
            ?>
            <div class="notice notice-error is-dismissible">
                <p><?php _e('Please fill in the FROM and TO date!', 'learndash'); ?></p>
            </div>
            <?php
        }
    }
}

add_action('init', 'request_passed_exams');

function array_to_csv(array &$array)
{
    if (count($array) == 0) {
        return null;
    }
    ob_start();
    $df = fopen("php://output", 'w');
    fputcsv($df, array_keys(reset($array)));
    foreach ($array as $row) {
        fputcsv($df, $row);
    }
    fclose($df);
    return ob_get_clean();
}

function init_file_download($filename) {
    // disable caching
    $now = gmdate("D, d M Y H:i:s");
    header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
    header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
    header("Last-Modified: {$now} GMT");

    // force download
    header("Content-Type: application/force-download");
    header("Content-Type: application/octet-stream");
    header("Content-Type: application/download");

    // disposition / encoding on response body
    header("Content-Disposition: attachment;filename={$filename}");
    header("Content-Transfer-Encoding: binary");

}