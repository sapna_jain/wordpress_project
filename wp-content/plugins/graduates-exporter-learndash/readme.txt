=== LearnDash Exporter ===

Contributors: mcostales84
Tags: learndash, graduates, exporter
Requires at least: 3.0.1
Tested up to: 4.9.4
License: GPLv2

Generate and export a csv report of graduated students in a specific period of time.

== Description ==

Generate and export a csv report of graduated students in a specific period of time. You need to have the LearnDash plugin in order to add this feature to your platform.

== Installation ==

To install this plugin just follow the standard procedure. Or you can upload the zip file to your server to the '/wp-content/plugins/'' directory, extract it and activate your plugin through the 'Plugins' menu in WordPress.

== Frequently Asked Questions ==

= Any question? =

Send me an email at mcostales@jumptoweb.com and I will answer you as soon as I can.

== Changelog ==

= 1.0 =
- Just launch the plugin!
