<?php

class LearnDashExporterAdminPage {

    public function __construct() {
        add_action('admin_menu', array($this, 'add_plugin_page'));
    }

    public function add_plugin_page() {
        add_menu_page(
            'LearnDash Exporter',
            'LearnDash Exporter',
            'export',
            'learndash-exporter',
            array($this, 'create_admin_page'),
            'dashicons-download'
        );
    }

    public function create_admin_page() {
        if (current_user_can('export')) {
            ?>

            <div class="wrap">
                <h1>LearnDash Exporter</h1>

                <form method="post">
                    <input type="hidden" name="action" value="ld-generate-csv"/>

                    <p class="ld-exporter-description">
                        Please, choose a date range to export a list of graduate students to a CSV file.
                    </p>
                    <label for="ld-date-from">From: </label>
                    <span class="ld-date-from">
                        <input placeholder="Date (from)" id="ld-date-from" class="ld-date"
                               type="datetime"
                               name="ld-date-from"/>
                    </span>
                    <label for="ld-date-to">To: </label>
                    <span class="ld-date-to">

                        <input placeholder="Date (to)" id="ld-date-to" class="ld-date"
                               type="datetime"
                               name="ld-date-to"/>
                    </span>
                    <input type="submit" name="submit" id="submit" class="button button-primary"
                           value="Generate Report">
                </form>
            </div>
            <?php
        } else {
            wp_die(__('You do not have sufficient permissions to access this page.'));
        }
    }

}