<?php
if ( ! defined( 'ABSPATH' ) ) {
	die();
}

$api      = $this->importiSDKSettings();
$verified = $i2sdk->getConfigurationOption( 'server_verified' ); ?>
	<div class="wrap">
		<p>
			Memberium works exclusively with Infusionsoft, just enter your Infusionsoft
			application name and api key below to get connected.
		</p>
		<p>
			If you need help finding your info, we've provided a guide at the link below,
			or you can contact us for assistance.
		</p>
		<p>
			<a href="http://ug.infusionsoft.com/article/AA-00442" target="_blank"><?php _e( 'Click here for help activating your Infusionsoft API Key in Infusionsoft' ); ?></a>
		</p>
		<p>
		</p>
		<h1>Infusionsoft Configuration</h1>
		<div style="width:800px;">
			<form method="POST" action="" autocomplete="off">
				<?php wp_nonce_field( plugin_basename( __FILE__ ), 'i2sdk_admin_api_nonce' ); ?>
				<h2>Infusionsoft <?php _e( 'API Settings' ); ?></h2>
				<table class="widefat" style="white-space:nowrap;">
					<tr>
						<td style="width:250px;"><?php _e( 'Infusionsoft App Name' ); ?>:</td>
						<td>https://<input id=lastpass-search-1 autocomplete=off type=text maxlength=32 size=20 name=i2sdk_app_name value="<?php echo $api['app_name']; ?>" style="text-align:right;" />.infusionsoft.com/</td>
					</tr>
					<tr>
						<td><label for=""><?php _e( 'Infusionsoft Encrypted key' ); ?>:</label></td>
						<td><input id="lastpass-search-2" maxlength="255" autocomplete="off" name="i2sdk_api_key" size="75" type="text" value="<?php echo $api['api_key']; ?>" ></td>
					</tr>
					<tr>
						<td><label for=""><?php _e( 'Current API status' ); ?>:</label></td>
						<td><?php echo ( $verified == 1 ) ? '<b style="color:green;">' . __( 'Verified' ) . '</b>' : '<b style="color:red;">' . $connection_failure . '</b>'; ?></td>
					</tr>
				</table>
				<p>
					<input type="submit" name="save" value="Save Infusionsoft API Configuration" class="button-primary" />
				</p>
			</form>
			<?php
			 if ( $verified ) {
					$unlocks['import'] = 1;
				echo '<form method="post" action="?page=memberium-installer-wizard&tab=import"><input type="submit" value="Next Step" class="button button-primary"></form>';
			 }
			?>
		</div>
	</div>
<?php
