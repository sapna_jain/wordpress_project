<?php
if ( ! defined( 'ABSPATH' ) ) {
	die();
}
$categories = $this->createCategory( 'Memberships', $this->getCategories() );

?>
<h2>Choose Your Membership Tag Category</h2>
<p>
	Please select an Infusionsoft tag category to create your membership tags in,
	you'll create the actual membership levels on the step after this.
</p>
<form action="?page=memberium-installer-wizard&tab=memberships" method="post">
	<p>
		Category Name:
		<select name="category_id" style="margin-bottom:15px;width:250px;">
			<?php
			foreach( $categories as $k => $v ) {
				$selected = ( stripos( $v, 'member' ) !== false );
				echo '<option value="', $k, '"', ( $selected ? ' selected="selected" ' : '' ), '>', $v, '</option>';
			}
			?>
		</select>
	</p>
	<input type="submit" name="save" value="Set Category" class="button-primary" />
</form>
<?php
