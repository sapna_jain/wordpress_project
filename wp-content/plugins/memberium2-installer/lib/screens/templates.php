<?php
if ( ! defined( 'ABSPATH' ) ) {
	die();
}

$message         = empty( $result ) ? '' : '<p class="protip"><strong>Success</strong>:  ' . $result . ' Templates Loaded</p>';
$unlocks['done'] = 1;

?>
<div style="max-width:1000px;">
<?php
	echo $message;
?>
<p>
	We've pre-created a set of sample pages for your membership site.
</p>
<p>
	You can click &ldquo;Load All Templates&rdquo; to add all the pages at once.
</p>
<p>
	If for any reason you <strong>don't</strong> wish to install the templates now, you can also skip this step for now, and load them later from within Memberium.
</p>
<p>
	The new templates will be loaded in <strong>draft mode</strong> so you can publish or delete them as desired.
	Once you've added the pages, you can set them as &ldquo;System Pages&rdquo; which will make them
	the default pages for certain actions to take place.
</p>
<form method="post" action="">
	<p>
		<input type="submit" name="save" class="button-primary" value="Load Templates">
	</p>
</form>
<form method="post" action="?page=memberium-installer-wizard&tab=done">
	<p>
		<input type="submit" name="next" class="button-primary" value="Skip this step">
	</p>
</form>
</div>
<style>
.specialthanks p {
	margin-left:20px;
}
.picturebios p {
	margin-top:20px;
	padding-top:20px;
	display:inline-block;
	vertical-align: top;
	width:340px;
	height:100px;
	clear:both;
}
.picturebios img {
	height:80px;
	width: 80px;
	float:left;
	margin-right:20px;
	margin-top:0px;
	border-radius:40px;
	-webkit-filter: grayscale(100%);
	filter: grayscale(100%);
}
.feature-section {
	width:100%;
}
.protip {
	margin-left:20px;
	margin-right:20px;
	padding:20px;
	padding-top:10px;
	padding-bottom:10px;
	padding-left:30px;
	background-color:#fff4cc;
	border-radius:10px;
	margin-bottom:12px;
}
</style>
<?php
