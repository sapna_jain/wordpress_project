<?php
if ( ! defined( 'ABSPATH' ) ) {
	die();
}


$unlocks['server'] = 1;
$environment = $this->getEnvironment();
$problems    = 0;

echo '<h2>Step 2</h2>';
?>
<p>
	Here are some details about your site, if everything is green you can click &ldquo;Next Step&rdquo;
	at the bottom.
</p>
<p>
	If there's anything in red, please contact our support team and we'd be happy to help!
</p>
<?php

echo '<table>';
if ( is_array( $environment['failures'] ) ) {
	foreach ( $environment['failures'] as $key => $values ) {
		echo '<tr>';
		echo '<td style="width:250px;">', $key, '</td>';
		echo '<td style="color:red;font-weight:bold;">', $values['message'], '</td>';
		echo '</tr>';
		$problems++;
	}
}

if ( is_array( $environment['required'] ) ) {
	foreach ( $environment['required'] as $key => $values ) {
		echo '<tr>';
		echo '<td style="width:250px;">', $key, '</td>';
		echo '<td style="font-weight:bold;color:green;">', $values['message'], '</td>';
		echo '</tr>';
	}
}
echo '<tr><td>&nbsp;</td></tr>';
echo '<tr><td><strong>Other Plugins</strong></td></tr>';
if ( is_array( $environment['detected'] ) ) {
	foreach ( $environment['detected'] as $key => $values ) {
		echo '<tr>';
		echo '<td style="width:250px;">', $key, '</td>';
		echo '<td>', $values['message'], '</td>';
		echo '</tr>';
	}
}
echo '</table>';
echo '&nbsp;<br>';
if ( $problems == 0 ) {
	$unlocks['i2sdk'] = 1;
	echo '<p style="color:green;"><strong>No problems detected.  Everything looks great!</strong></p>';
	echo '<form method="post" action="?page=memberium-installer-wizard&tab=i2sdk"><input type="submit" value="Next Step" class="button button-primary"></form>';
}
else {
	$unlocks['i2sdk'] = 0;
	echo '<p style="color:red;"><strong>', $problems, ' problems detected.<br><br>This needs to be corrected before we can continue.</strong></p>';
	echo '<form method="post"><input type="submit" value="Re-Scan System" class="button button-primary"></form>';
}

if ( $problems ) {
	$unlocks['categories'] = 0;
}