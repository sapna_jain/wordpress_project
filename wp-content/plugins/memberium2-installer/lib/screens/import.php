<?php
if ( ! defined( 'ABSPATH' ) ) {
	die();
}

$unlocks['categories'] = 1;
?>
<h1>Settings Importer</h1>
<p>
	If you've used a membership system previously we may be able to pull some settings in for you.
</p>
<p>
	If there are no settings to import, simply click "Next Step" to continue.
</p>
<?php

if ( $this->hasiMember360Settings() ){
	if ( $_SERVER['REQUEST_METHOD'] == 'POST' ) {
		if ( $_POST['save'] == 'Import iMember360 Settings' ) {
			echo '<textarea rows=15 style="width:90%;margin-top:10px;margin-bottom:10px;">', $this->importiMember360Settings(), '</textarea>';
		}
	}

	echo '<form method="post">';
	echo '<input type="submit" name="save" value="Import iMember360 Settings" class="button-primary" />';
	echo '</form>';
}
else {
	echo '<p>There were no settings found to import</p>';
}

echo '<form style="margin-top:10px;" method="post" action="?page=memberium-installer-wizard&tab=categories"><input type="submit" value="Next Step" class="button button-primary"></form>';
