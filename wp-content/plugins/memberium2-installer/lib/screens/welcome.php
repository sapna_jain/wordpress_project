<?php
if ( ! defined( 'ABSPATH' ) ) {
	die();
}
?>
<style>
.specialthanks p {
	margin-left:20px;
}
.picturebios p {
	margin-top:20px;
	padding-top:20px;
	display:inline-block;
	vertical-align: top;
	width:340px;
	height:100px;
	clear:both;
}
.picturebios img {
	height:80px;
	width: 80px;
	float:left;
	margin-right:20px;
	margin-top:0px;
	border-radius:40px;
	-webkit-filter: grayscale(100%);
	filter: grayscale(100%);
}
.feature-section {
	width:100%;
}
.protip {
	margin-left:20px;
	margin-right:20px;
	padding:20px;
	padding-top:10px;
	padding-bottom:10px;
	padding-left:30px;
	background-color:#fff4cc;
	border-radius:10px;
	margin-bottom:12px;
}
</style>

<div style="overflow:hidden;">
	<div style="float:left;display:inline-block;margin-left:35px;width:700px;">
		<p><strong>
			Congratulations!
		</strong></p>
		<p>
			By installing Memberium you’ve just joined the happiest plugin team on Earth!
		</p>
		<p>
			We'd like to welcome you to the family and say thanks again, you're going to love it.
			This part of the process will walk you through setting up some of the basics but you can
			<em>always</em> contact our support team if you would like help.
		</p>
		<div class="protip">
			<p>
				<strong>Pro Tip</strong><br>
				If at any point in working with Memberium, you have questions, concerns, or would simply like assistance, please email <a target="_blank" href="https://memberium.com/support/">support</a> 7 days a week.
			</p>
			<p>
				If you'd like to discuss strategy and concepts, you can also jump on one of our live <a target="_blank" href="https://memberium.com/office-hours">Office Hours</a> calls.
			</p>
		</div>
		<p>
			Please take just a moment and watch the video on this page to get a quick orientation,
			when you're finished click the &ldquo;Get Started&rdquo; button below it to continue.
		</p>
		<div style="clear:both;"></div>
		<div class="feature-section images-stagger-right picturebios">
			<p>
				<img src="https://memberium.com/members/wp-content/uploads/2014/10/Micah-Mitchell1.png" alt="Micah Mitchell">
				<strong>Micah Mitchell</strong><br />
				Co-Founder<br />
			</p>
			<p>
				<a href="https://www.linkedin.com/in/davidjbullock"><img src="//memberium.com/members/wp-content/uploads/2014/10/dave-bullock-infusionsoft.png" alt="David Bullock"></a>
				<strong>David Bullock</strong><br />
				Co-Founder<br />
			</p>
		</div>
		<p>
			&mdash; Micah and Dave
		</p>
		<div style="clear:both;"></div>
	</div>

	<div style="float:left;width:430px;display:inline-block;">
		<div align='center'>
			<iframe src="//player.vimeo.com/video/164371815" width="420" height="275" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

			<p>
				Next, join the Facebook Membership Group...
			</p>
			<a href="https://www.facebook.com/groups/Memberium.Support/" target="_blank" class="button button-primary">Join Memberium on Facebook</a>
			<p>
				...Then, hit the &ldquo;Get Started&rdquo; button below...
			</p>
			<form method="post" action="?page=memberium-installer-wizard&tab=server"><input type="submit" value="Get Started" class="button button-primary"></form>
		</div>
	</div>

</div>
<div style="clear:both;"></div>
