<?php
if ( ! defined( 'ABSPATH' ) ) {
	die();
}

?>
<div style="max-width:1100px;">
	<p>
		Holy Cow!
	</p>
	<p>
		That was easy.  Now you're ready to create your campaign in Infusionsoft.
	</p>
	<p>
		Watch the short video below and click &ldquo;Activate Memberium&rdquo; at the bottom
		when you&rsquo;re done.
	</p>
	<form method="post" action="?page=memberium-installer-wizard&tab=done">
		<input type="submit" name="save" class="button-primary" value="Activate Memberium">
	</form>
	<div align="center">
		<iframe src="//player.vimeo.com/video/164376145" width="960" height="720" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
	</div>


</div>
<?php

