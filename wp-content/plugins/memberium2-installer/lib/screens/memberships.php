<?php
if ( ! defined( 'ABSPATH' ) ) {
	die();
}

$memberships = $this->getMemberships();
$tags        = $this->getTags();
$skip_button = count( $memberships ) ? 'Finish and Go to Next Section' : 'Skip to Next Section';

?>
<div style="max-width:1100px;">
	<h3>Create New Membership Levels</h3>
<p>
	When you create a new membership level below, it will create one or more tags in Infusionsoft
	that you can assign to contacts to give them that level of access as well as create a membership
	level here on your site that you can use to protect content.
</p>
<p>
	 When users have those tags, they'll have access to the content protected by those membership levels.
</p>
<form method="post" action="">
	<input type="hidden" name="is_subscription" value="0">
	<p>
		Membership Name:  <input type="text" name="membership_name" size="40" value="" style="margin-right:30px;" placeholder="Enter the name of your membership level" required>
		Is Subscription?  <input type="checkbox" name="is_subscription" value="1">
	</p>
	<input type="submit" name="save" value="Create New Membership" class="button-primary" />
</form>
<form action="?page=memberium-installer-wizard&tab=templates" method="post" style="margin-top:20px;">
	<input type="submit" value="<?php echo $skip_button ?>" class="button-primary">
</form>
<?php

if ( is_array( $memberships ) && count( $memberships ) ) {
	echo '<table class="widefat fixed" style="margin-top:20px;">';
	echo '<tr>';
	echo '<td><strong>Membership Name</strong></td>';
	echo '<td><strong>Membership Tag</strong></td>';
	echo '</tr>';
	foreach( $memberships as $membership ) {
		echo '<tr>';
		echo '<td>', $membership['name'], '</td>';
		echo '<td>', $tags[$membership['main_id']], ' (', $membership['main_id'], ')</td>';
		echo '</tr>';
	}
	if ( count( $memberships ) > 0 ) {
		$unlocks['templates'] = 1;
	}
	echo '</table>';
}
else {
	echo '<p>No Memberships found</p>';
}
echo '</div>';