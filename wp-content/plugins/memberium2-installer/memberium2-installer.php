<?php
/*
Plugin Name: Memberium Installer
Plugin URI: http://www.webpowerandlight.com
Description: Installs Memberium and resources
Version: 1.3
Author: David Bullock
Author URI: http://www.webpowerandlight.com/
License: GPL2
*/


if ( ! defined( 'ABSPATH' ) ) {
	die();
}

if ( is_admin() ) {
	define( 'MEMBERIUM_INSTALLER_HOME', __FILE__ );
	define( 'MEMBERIUM_INSTALLER_LIB', dirname( __FILE__ ) . '/lib/' );

	require_once dirname( __FILE__ ) . '/lib/core.php';
}
