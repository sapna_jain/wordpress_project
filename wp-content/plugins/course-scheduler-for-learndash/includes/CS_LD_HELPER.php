<?php
namespace CSLD;
/**
 * Abort if this file is accessed directly.
 */
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

class CS_LD_HELPER {

    /**
     *  Constructor
     */
    public function __construct() {
        add_action ( 'admin_enqueue_scripts', array( $this, 'ldccPluginScripts' ) );
        add_action ( 'admin_menu', array( $this, 'ldccAdminMenu' ), 1001 );
        add_action ( 'wp_ajax_add_course_schedule', array( $this, 'addCourseSchedule' ) );
        add_action ( 'wp_ajax_remove_course_schedule', array( $this, 'removeCourseSchedule' ) );
        add_action ( 'wp_ajax_getEvents', array( $this, 'getEvents' ) );
        add_action ( 'pre_get_posts', array( $this, 'alter_query' ) );
        add_action ( 'current_screen', array( __CLASS__, 'add_help_tab' ) );
        add_action ( 'admin_notices', array( __CLASS__, 'add_help_notification' ) );
        add_action ( 'current_screen', array( __CLASS__, 'save_csld_settings' ) );
        add_filter ( 'admin_footer_text', array( $this, 'remove_footer_admin' ) );
        add_shortcode ( 'cs_scheduled_dates', array( $this, 'get_specified_dates' ) );
    }

    /**
     * Alter global query to show courses
     *
     * @param $query
     * @return mixed
     */
    public function alter_query( $query ) {
        
        if( is_admin() )
            return $query;

        if ( ! $query->is_main_query(  ) )
            return $query;

        if ( isset( $query->query_vars['post_type'] ) && 
            ( 'sfwd-courses' == $query->query_vars['post_type'] ||
                'sfwd-lessons' == $query->query_vars['post_type'] ||
                'sfwd-topic' == $query->query_vars['post_type'] ||
                'sfwd-quiz' == $query->query_vars['post_type'] ) ) {

            $courses_args = array( 'posts_per_page' => -1, 'post_type' => 'sfwd-courses', 'post_status'   => 'publish' );
            $courses = get_posts( $courses_args );
            
            $post_in = array();
            $show_option = get_option( 'course_scheduler_ld_addon_setting' );
            $show_courses = ! empty( $show_option ) ? $show_option : 0 ;
            if( is_array( $courses ) ) {
                
                foreach( $courses as $course ) {
                    $data = get_post_meta( $course->ID, 'course_schedule', true );
                    if( is_array( $data ) ) {
                        if( empty( $data ) ) {
                            $post_in[] = $course->ID;
                        }

                        foreach( $data as $arr ) {
                            if( '1' == $show_courses ) {
                                if( date( 'Y-m-d' ) == $arr ) {
                                    $post_in[] = $course->ID;
                                }
                            }
                        }
                    }
                }

                if( ! is_single(  ) ) {

                    if( "1" == $show_courses ) {
                        $query->set( "post__in", $post_in );
                    } else {
                        if( sizeof( $courses ) != sizeof($post_in) ) {
                            $query->set( "post__not_in", $post_in );
                        }
                    }
                }
            }

            if( is_single(  ) ) {
                $show_courses = !empty( $show_option ) ? $show_option : 0 ;
                $post_type = $query->query_vars['post_type']; 

                $slug = $query->query[$post_type];
                $post_obj = get_page_by_path( $slug , OBJECT, $post_type );
                
                if( !isset( $post_obj->ID ) ) {
                    return $query;
                }

                $post_id = $post_obj->ID;

                if( 'sfwd-courses' == $post_type ) {

                    $this->ldccShowHide( $post_id, $show_courses, $post_in, "Course" );

                } elseif( 'sfwd-lessons' == $post_type ) {

                    $lesson_id = $post_id;
                    $course_dates = get_post_meta( $lesson_id, "_sfwd-lessons", true );
                    $course_id = $course_dates["sfwd-lessons_course"];
                    $this->ldccShowHide( $course_id, $show_courses, $post_in, "Lesson" );

                } elseif( 'sfwd-quiz' == $post_type ) {

                    $lesson_id = $post_id;
                    $course_dates = get_post_meta( $lesson_id, "_sfwd-quiz", true );

                    $course_id = $course_dates["sfwd-quiz_course"];
                    $this->ldccShowHide( $course_id, $show_courses, $post_in, "Quiz" );

                } elseif( 'sfwd-topic' == $post_type ) {

                    $lesson_id = $post_id;
                    $course_dates = get_post_meta( $lesson_id, "_sfwd-topic", true );
                    $course_id = $course_dates["sfwd-topic_course"];
                    $this->ldccShowHide( $course_id, $show_courses, $post_in, "Topic" );

                }
            }
            return $query;
        }
        return $query;
    }

    /**
     * Show/Hide posts
     *
     * @param $course_id
     * @param $show_courses
     * @param $post_in
     * @param $post_type
     */
    public function ldccShowHide( $course_id, $show_courses, $post_in, $post_type ) {

        global $type;
        $type = $post_type;
        $date_today = date( 'Y-m-d' );

        //Remove courses scheduled on old dates first
        $course_dates = get_post_meta( $course_id, 'course_schedule', true );
       
        if( $course_dates ) {
            foreach ($course_dates as $key => $course_date) {
                if ( $date_today > $course_date ) {
                    unset( $course_dates[$key] );
                }
            }
            update_post_meta( $course_id, "course_schedule", $course_dates );
        }

        $course_dates = get_post_meta( $course_id, 'course_schedule', true );
        if( '1' != $show_courses ) {
            if( in_array( $date_today, $course_dates ) ) {
                if( !in_array( $course_id, $post_in ) ) {
                    add_filter('learndash_content', function() {
                        global $type;
                        $message = get_option( "cs_ld_addon_hide_". strtolower($type) ."_message" );
                        return do_shortcode( esc_textarea( $message ) );
                    });
                }
            }
        } else {
            $data = get_post_meta( $course_id, 'course_schedule', true );
            $to_show = true;
            if( is_array( $data ) ) {
                foreach( $data as $arr ) {
                    if( $date_today == $arr ) {
                        $to_show = false;
                    }
                }
            }
            
            if ( !empty ( $data ) ) {
                if ( $to_show ) {
                    add_filter('learndash_content', function() {
                        global $type;
                        $message = get_option( "cs_ld_addon_show_". strtolower($type) ."_message" );
                        return do_shortcode( esc_textarea( $message ) );
                    });
                }
            }
        }
    }

    /**
     * Enqueue scritps for plugin
     */
    public function ldccPluginScripts() {
        $screen = get_current_screen();

        if( isset($screen->id) && ( "toplevel_page_calendar_course" == $screen->id || "course-scheduler_page_calendar_course_settings" == $screen->id ) ) {
            wp_enqueue_script( 'moment', CS_LD_PLUGIN_URL . "/assets/js/moment.min.js", array( 'jquery' ), false );
            wp_enqueue_script( 'fullcalendar-js', CS_LD_PLUGIN_URL . "/assets/js/fullcalendar.min.js", array( 'moment' ), false );
            wp_enqueue_script( 'calendar-color', CS_LD_PLUGIN_URL . "/assets/js/calendar-color.js", array( 'fullcalendar-js', 'jquery-ui-tabs', 'jquery-ui-draggable', 'jquery-ui-droppable'  ), false );

            wp_localize_script( 'calendar-color', 'LDCSAdminVars', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
            wp_enqueue_style( 'jquery-ui', CS_LD_PLUGIN_URL . "/assets/css/jquery-ui.css", array() );
            wp_enqueue_style( 'fullcalendar', CS_LD_PLUGIN_URL . "/assets/css/fullcalendar.css", array() );
        }
    }

    /**
     * Rendered added events on calender
     */
    public function getEvents() {
        $date_year = sanitize_text_field(  $_POST['year'] );

        $args = array( 'posts_per_page' => -1, 'post_type' => 'sfwd-courses', 'post_status'   => 'publish' );
        $courses = get_posts( $args );
        $loop_num = 0;
        $course_schedules = array();
        foreach( $courses as $course )  {
            $data = get_post_meta( $course->ID, 'course_schedule', true );
            foreach( $data as $arr )  {
                $schd_date = explode( "-", $arr );
                if( $date_year === $schd_date[0] ) {
                    $course_schedules[$loop_num]['course_id'] = $course->ID;
                    $course_schedules[$loop_num]['title'] = $course->post_title;
                    $course_schedules[$loop_num]['start'] = $arr;
                    $loop_num++;
                } else {
                    continue;
                }                
            }
        }
        
        $return = $course_schedules;
        wp_send_json( $return );
    }

    /**
     * Schedule the courses
     */
    public function addCourseSchedule() {
        $date_v = explode( "-", $_POST["date"] );
        $date_validated = wp_checkdate( $date_v[1], $date_v[2], $date_v[0], "Y-m-d" );
        if ( !$date_validated ) {
            exit( "wrong format" );
        }

        $date = $_POST['date'];
        if( date( "Y-m-d" ) > $date ) {
            exit( "false" );
        }

        $p_date_v = explode( "-", $_POST["prev_date"] );
        $p_date_validated = wp_checkdate( $p_date_v[1], $p_date_v[2], $p_date_v[0], "Y-m-d" );
        if ( $p_date_validated ) {
            $prev_date = $_POST['prev_date'];
        }

        $course_id = intval( $_POST['course_id'] );
        $data = get_post_meta( $course_id, 'course_schedule', true );
        if( ''  == $data ) {
            $data = array();
        }

        if( !empty( $prev_date ) ) 
            $key = array_search( $prev_date, $data );

        $remove = ( isset( $_POST['remove_date'] ) ) ? $_POST['remove_date'] : "";
        if( !empty( $remove ) && $remove !== false ) {
            if( $key !== false ) {
                unset( $data[$key] );
            }
        } 

        $data[] = $date;
        $data = array_unique( $data );   
        update_post_meta( $course_id, 'course_schedule', $data );
        exit;
    }

    /**
     * Remove courses from schedule
     */
    public function removeCourseSchedule() {
        $date_v = explode( "-", $_POST["date"] );
        $date_validated = wp_checkdate( $date_v[1], $date_v[2], $date_v[0], "Y-m-d" );
        if ( !$date_validated ) {
            exit( "false" );
        }

        $date = $_POST['date'];
        $course_id = intval( $_POST['course_id'] );
        $data = get_post_meta( $course_id, 'course_schedule', true );

        $key = array_search( $date, $data );       
        if( $key !== false ) {
            unset( $data[$key] );
        }

        update_post_meta( $course_id, 'course_schedule', $data );
        exit;
    }

    /**
     * Adds Reporting Chart menu page
     */
    public function ldccAdminMenu() {

        /**
         * Add Course scheduler Page
         */
        add_menu_page(
            __( 'Course Scheduler', 'cs_ld_addon' ),
            'Course Scheduler',
            'manage_options',
            'calendar_course',
            array( $this, ( 'csView' ) ),
            'dashicons-calendar-alt'
        );

        /**
         * Add Setting Page
         */
        add_submenu_page(
            'calendar_course',
            'Calendar',
            'Calendar',
            'manage_options',
            'calendar_course',
            array( $this, ( 'csView' ) )
        );

        /**
         * Add Setting Page
         */
        add_submenu_page(
            'calendar_course',
            'Settings',
            'Settings',
            'manage_options',
            'calendar_course_settings',
            array( $this, ( 'csViewSetting' ) )
        );
    }

    /**
     * Loads calender view
     */
    public function csView() {
        $args = array( 'posts_per_page' => -1, 'post_type' => 'sfwd-courses', 'post_status'   => 'publish' );
        $courses = get_posts( $args );
        if( file_exists( dirname(__FILE__) . '/views/calendar_view.php' ) ) {
            require_once( dirname(__FILE__) . '/views/calendar_view.php' );
        }
    }

    /**
     * Loads settings view
     */
    public function csViewSetting() {
        if( file_exists( dirname(__FILE__) . '/views/calendar_settings_view.php' ) ) {
            require_once( dirname(__FILE__) . '/views/calendar_settings_view.php' );
        }
    }

    /**
     * Add Help Tab
     */
    public static function add_help_tab() {
        $screen = get_current_screen();
        if( $screen->base !== "toplevel_page_calendar_course" )
            return;

        $screen->add_help_tab( array(
            "id"	    => "cs-ld-course-scheduler-details",
            "title"	    => __( "Course Scheduler", "cs_ld_addon" ),
            "content"	=>
                "<p>" . __( "Drag published courses on left to the calender to schedule them on specific dates or show them on all dates except the ones added on the calendar, this is based on the settings saved for the addon.", "cs_ld_addon" ) . "</p>" .
                "<p>" . __( "Each course can be dragged and dropped on the calendar multiple times and multiple courses can be scheduled for the same date.", "cs_ld_addon" ) . "</p>" .
                 "<p>" . __( "Click the cross icon to remove the course from calendar, you also have the option to move the dropped courses to other dates on the calendar by simply drag and drop from the previous date to the new date" ) . "</p>".
                "<p>" . __( "Already dropped courses would always show up on the calendar", "cs_ld_addon" ) . "</p>"
        ) );
    }

    /**
     * Add footer branding
     *
     * @param $footer_text
     * @return mixed
     */
    function remove_footer_admin ( $footer_text ) {
        if( isset( $_GET["page"] ) && ( $_GET["page"] == "calendar_course_settings" || $_GET["page"] == "calendar_course" ) ){
            _e('Fueled by <a href="http://www.wordpress.org" target="_blank">WordPress</a> | developed and designed by <a href="https://wooninjas.com" target="_blank">The WooNinjas</a></p>');
        } else {
            return $footer_text;
        }
    }

    /**
     * Show info notification on calender page
     */
    public static function add_help_notification () {
        $screen = get_current_screen();
        
        if( isset( $_GET['csld_dismiss_notice'] ) ) {
            update_user_meta(get_current_user_id(), "csld_review_dismissed", 1);
        }

        if( isset($screen->id) && $screen->id === "toplevel_page_calendar_course" ) {
            ?>
            <div class="notice notice-info" style="margin-top:50px;">
                <p><?php _e( 'Drag and drop courses from the left on the calendar to assign it <strong>on specific dates</strong> OR <strong>except the specific dates</strong> depending on your <a href="'.admin_url().'admin.php?page=calendar_course_settings">settings</a> here', "cs_ld_addon" ); ?></p>
            </div>
            <div class="notice notice-error invalid-date" style="display:none;margin-top:20px;">
                <p><?php _e( 'You cannot schedule a course for a past date.', "cs_ld_addon" ); ?></p>
            </div>
            <?php
        } else {
            $user_data = get_userdata(get_current_user_id());
            $csld_review_dismissed = get_user_meta(get_current_user_id(), "csld_review_dismissed", true);
            $dismiss_url = add_query_arg( 'csld_dismiss_notice', 1 );

            if ( ! $csld_review_dismissed ) {
            ?>
             <div class="notice notice-info">
                <?php _e('<p>Hi <strong>' . $user_data->user_nicename . '</strong>, thankyou for using Course Scheduler for LearnDash If you find our plugin useful kindly take some time to leave a review and a rating for us <a href="https://wooninjas.com/wn-products/learndash-course-scheduler/" target="_blank" ><strong>here</strong></a> </p><p><a href="'.esc_attr($dismiss_url).'">Dismiss Notice</a></p>', "csld"); ?>
            </div>
            <?php
            }
        }
    }

    /**
     * Return specified dates for
     * Courses, Lessons, Quizzes, Topics
     *
     * @return bool
     */
    public function get_specified_dates() {

        global $post;
        $post_id = $post->ID;
        $course_dates = "";

        if( $post->post_type == "sfwd-courses" ) {

            $course_dates = get_post_meta( $post_id, "course_schedule", true );

        } elseif( $post->post_type == "sfwd-lessons" ) {

            $lesson_details = get_post_meta( $post_id, "_sfwd-lessons", true );
            $course_id = $lesson_details["sfwd-lessons_course"];
            $course_dates = get_post_meta( $course_id, "course_schedule", true );

        } elseif( $post->post_type == "sfwd-topic" ) {

            $topic_details = get_post_meta( $post_id, "_sfwd-topic", true );
            $course_id = $topic_details["sfwd-topic_course"];
            $course_dates = get_post_meta( $course_id, "course_schedule", true );

        } elseif( $post->post_type == "sfwd-quiz" ) {

            $quiz_details = get_post_meta( $post_id, "_sfwd-quiz", true );
            $course_id = $quiz_details["sfwd-quiz_course"];
            $course_dates = get_post_meta( $course_id, "course_schedule", true );

        }

        // No Course Dates Set
        if( ! $course_dates ) {
            return false;
        }

        $available_dates = '<ul>';
        $format = get_option( 'date_format' );
        foreach( $course_dates as $key => $course_date ) {
            if( date( 'Y-m-d' ) <= $course_date ) {
                $new_date = date( $format , strtotime( $course_date ) );
                $available_dates .= "<li>" . $new_date . "</li>";
            }
        }

        $available_dates .= '</ul>';
        $available_course_date = rtrim( $available_dates, " ," );
        return $available_course_date;
    }

    /**
     * Save Plugin's Settings
     */
    public static function save_csld_settings () {

        $screen = get_current_screen();
        if( $screen->id === "course-scheduler_page_calendar_course_settings" ) {
            if( current_user_can( "manage_options" ) ) {
                if ( ! empty( $_POST ) && check_admin_referer( 'csld_settings', 'csld_settings_field' ) ) {
                    if( $_POST['show_courses'] == "1" || $_POST['show_courses'] == "0" ) {
                        update_option( 'course_scheduler_ld_addon_setting', $_POST['show_courses'] );
                    }

                    update_option( 'cs_ld_addon_show_course_message', sanitize_textarea_field( $_POST['show_courses_message'] ) );
                    update_option( 'cs_ld_addon_hide_course_message', sanitize_textarea_field( $_POST['hide_courses_message'] ) );
                    update_option( 'cs_ld_addon_show_lesson_message', sanitize_textarea_field( $_POST['show_lesson_message'] ) );
                    update_option( 'cs_ld_addon_hide_lesson_message', sanitize_textarea_field( $_POST['hide_lesson_message'] ) );
                    update_option( 'cs_ld_addon_show_quiz_message', sanitize_textarea_field( $_POST['show_quiz_message'] ) );
                    update_option( 'cs_ld_addon_hide_quiz_message', sanitize_textarea_field( $_POST['hide_quiz_message'] ) );
                    update_option( 'cs_ld_addon_show_topic_message', sanitize_textarea_field( $_POST['show_topic_message'] ) );
                    update_option( 'cs_ld_addon_hide_topic_message', sanitize_textarea_field( $_POST['hide_topic_message'] ) );
                }
            }
        }
    }
}