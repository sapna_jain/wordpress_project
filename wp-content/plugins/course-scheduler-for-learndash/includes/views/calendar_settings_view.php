<?php
namespace CSLD;
/**
 * Abort if this file is accessed directly.
 */
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

$show_options = get_option( 'course_scheduler_ld_addon_setting' );
$show_courses = !empty( $show_options ) ? $show_options : 0 ;
$show_courses_message_option = get_option( 'cs_ld_addon_show_course_message' );
$show_courses_message = !empty( $show_courses_message_option ) ? $show_courses_message_option : "The course will be available on below date/s: [cs_scheduled_dates]" ;
$hide_courses_message_option = get_option( 'cs_ld_addon_hide_course_message' );
$hide_courses_message = !empty( $hide_courses_message_option ) ? $hide_courses_message_option : "The course will not be available on below date/s: [cs_scheduled_dates] " ;
$show_lesson_message_option = get_option( 'cs_ld_addon_show_lesson_message' );
$show_lesson_message = !empty( $show_lesson_message_option ) ? $show_lesson_message_option : "The lesson will be available on below date/s: [cs_scheduled_dates]" ;
$hide_lesson_message_option = get_option( 'cs_ld_addon_hide_lesson_message' );
$hide_lesson_message = !empty( $hide_lesson_message_option ) ? $hide_lesson_message_option : "The lesson will not be available on below date/s: [cs_scheduled_dates]" ;
$show_quiz_message_option = get_option( 'cs_ld_addon_show_quiz_message' );
$show_quiz_message = !empty( $show_quiz_message_option ) ? $show_quiz_message_option : "The quiz will be available on below date/s: [cs_scheduled_dates]" ;
$hide_quiz_message_option = get_option( 'cs_ld_addon_hide_quiz_message' );
$hide_quiz_message = !empty( $hide_quiz_message_option ) ? $hide_quiz_message_option : "The quiz will not be available on below date/s: [cs_scheduled_dates]" ;
$show_topic_message_option = get_option( 'cs_ld_addon_show_topic_message' );
$show_topic_message = !empty( $show_topic_message_option ) ? $show_topic_message_option : "The topic will be available on below date/s: [cs_scheduled_dates]" ;
$hide_topic_message_option = get_option( 'cs_ld_addon_hide_topic_message' );
$hide_topic_message = !empty( $hide_topic_message_option ) ? $hide_topic_message_option : "The topic will not be available on below date/s: [cs_scheduled_dates]" ;
?>
<body>
    <div id="wrap" class="cs_settings_wrapper">
        <div class="padding">
            <h2 id="cs-ld-addon-setting-heading">
                <span class="dashicons dashicons-admin-tools"></span>
                <?php echo __( 'Course scheduler Settings', 'cs_ld_addon' ); ?>
            </h2>
            <form method="post" action="">
                <div id="setting_tabs">
                    <ul>
                        <li><a href="#general_settings"><?php _e('General Settings', 'cs_ld_addon'); ?></a></li>
                        <li><a href="#course_settings"><?php _e('Course Message', 'cs_ld_addon'); ?></a></li>
                        <li><a href="#lesson_settings"><?php _e('Lesson Message', 'cs_ld_addon'); ?></a></li>
                        <li><a href="#topic_settings"><?php _e('Topic Message', 'cs_ld_addon'); ?></a></li>
                        <li><a href="#quiz_settings"><?php _e('Quiz Message', 'cs_ld_addon'); ?></a></li>
                    </ul>
                    <div id="general_settings">
                        <div class="ee-table-wrap">
                            <table class="setting-table-wrapper">
                                <tbody>
                                    <tr>
                                        <td width="40px" align="center">
                                            <input type="radio" id="show_courses_on_schedule" name="show_courses" value="1" <?php if( $show_courses == 1 ) echo 'checked="checked"'; ?>>
                                        </td>
                                        <td align="left">
                                            <label for="maintenance_mode_level_off">
                                                <strong><?php _e('Show courses on specified dates', 'cs_ld_addon'); ?></strong>
                                            </label>
                                            <p class="description" style="font-weight: normal;">
                                                <?php _e('Set this option if you want to show the courses "only" on the dates set on the calendar', 'cs_ld_addon'); ?>
                                            </p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="40px" align="center">
                                            <input type="radio" id="do_not_show_courses_on_schedule" name="show_courses" value="0" <?php if( $show_courses == 0 ) echo 'checked="checked"'; ?>>
                                        </td>
                                        <td align="left">
                                            <label for="maintenance_mode_level_on"><strong><?php echo __('Show courses except the specified dates', 'cs_ld_addon'); ?></strong></label>
                                            <p class="description" style="font-weight: normal;">
                                                <?php _e('Set this option if you want to show the courses "except" the dates set on the calendar', 'cs_ld_addon' ); ?>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div id="course_settings">
                        <table class="setting-table-wrapper">
                            <tbody>
                                <tr>
                                    <td colspan="2">
                                        <h3 class="message_page_headings"><?php echo __( "Course Page Messages:", "cs_ld_addon" ); ?></h3>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="40px" align="center">
                                        <?php wp_editor( esc_textarea( $show_courses_message ), "show_courses_message", $settings = array() ); ?>
                                    </td>
                                    <td align="left">
                                        <label for="maintenance_mode_level_off">
                                            <strong><?php _e('Message when "Show courses on specified dates" setting enabled', 'cs_ld_addon'); ?></strong>
                                        </label>
                                        <p class="description" style="font-weight: normal;">
                                            <?php _e('This message will be shown on frontend when user visits course page and that course has been scheduled for a later date with the first setting option being selected in the general settings. You can use <strong>[cs_scheduled_dates]</strong> to get the scheduled dates', 'cs_ld_addon'); ?>
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="40px" align="center">
                                        <?php wp_editor( esc_textarea( $hide_courses_message ), "hide_courses_message", $settings = array() ); ?>
                                    </td>
                                    <td align="left">
                                        <label for="maintenance_mode_level_off">
                                            <strong><?php _e('Message when "Show courses except the specified dates" setting enabled', 'cs_ld_addon'); ?></strong>
                                        </label>
                                        <p class="description" style="font-weight: normal;">
                                            <?php _e('This message will be shown on frontend when user visits course page and that course has been scheduled for dates except the ones set on calendar, with the second setting option being selected in the general settings. You can use <strong>[cs_scheduled_dates]</strong> to get the scheduled dates', 'cs_ld_addon'); ?>
                                        </p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div id="lesson_settings">
                        <table class="setting-table-wrapper">
                            <tbody>
                                <tr>
                                    <td colspan="2">
                                        <h3 class="message_page_headings"><?php echo __( "Lesson Page Messages:", "cs_ld_addon" ); ?></h3>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="40px" align="center">
                                        <?php wp_editor( esc_textarea( $show_lesson_message ), "show_lesson_message", $settings = array() ); ?>
                                    </td>
                                    <td align="left">
                                        <label for="maintenance_mode_level_off">
                                            <strong><?php _e('Message when "Show courses on specified dates" setting enabled', 'cs_ld_addon'); ?></strong>
                                        </label>
                                        <p class="description" style="font-weight: normal;">
                                            <?php _e('This message will be shown on frontend when user visits lesson page and that course has been scheduled for a later date with the first setting option being selected in the general settings. You can use <strong>[cs_scheduled_dates]</strong> to get the scheduled dates', 'cs_ld_addon'); ?>
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="40px" align="center">
                                        <?php wp_editor( esc_textarea( $hide_lesson_message ), "hide_lesson_message", $settings = array() ); ?>
                                    </td>
                                    <td align="left">
                                        <label for="maintenance_mode_level_off">
                                            <strong><?php _e('Message when "Show courses except the specified dates" setting enabled', 'cs_ld_addon'); ?></strong>
                                        </label>
                                        <p class="description" style="font-weight: normal;">
                                            <?php _e('This message will be shown on frontend when user visits lesson page and that course has been scheduled for dates except the ones set on calendar, with the second setting option being selected in the general settings. You can use <strong>[cs_scheduled_dates]</strong> to get the scheduled dates', 'cs_ld_addon'); ?>
                                        </p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div id="quiz_settings">
                        <table class="setting-table-wrapper">
                            <tbody>
                                <tr>
                                    <td colspan="2">
                                        <h3 class="message_page_headings"><?php echo __( "Quiz Page Messages:", "cs_ld_addon" ); ?></h3>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="40px" align="center">
                                        <?php wp_editor( esc_textarea( $show_quiz_message ), "show_quiz_message", $settings = array() ); ?>
                                    </td>
                                    <td align="left">
                                        <label for="maintenance_mode_level_off">
                                            <strong><?php _e('Message when "Show courses on specified dates" setting enabled', 'cs_ld_addon'); ?></strong>
                                        </label>
                                        <p class="description" style="font-weight: normal;">
                                            <?php _e('This message will be shown on frontend when user visits quiz page and that course has been scheduled for a later date with the first setting option being selected in the general settings. You can use <strong>[cs_scheduled_dates]</strong> to get the scheduled dates', 'cs_ld_addon'); ?>
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="40px" align="center">
                                        <?php wp_editor( esc_textarea( $hide_quiz_message ), "hide_quiz_message", $settings = array() ); ?>
                                    </td>
                                    <td align="left">
                                        <label for="maintenance_mode_level_off">
                                            <strong><?php _e('Message when "Show courses except the specified dates" setting enabled', 'cs_ld_addon'); ?></strong>
                                        </label>
                                        <p class="description" style="font-weight: normal;">
                                            <?php _e('This message will be shown on frontend when user visits quiz page and that course has been scheduled for dates except the ones set on calendar, with the second setting option being selected in the general settings. You can use <strong>[cs_scheduled_dates]</strong> to get the scheduled dates', 'cs_ld_addon'); ?>
                                        </p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div id="topic_settings">
                        <table class="setting-table-wrapper">
                            <tbody>
                                <tr>
                                    <td colspan="2">
                                        <h3 class="message_page_headings"><?php echo __( "Topic Page Messages:", "cs_ld_addon" ); ?></h3>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <?php wp_editor( esc_textarea( $show_topic_message ), "show_topic_message", $settings = array() ); ?>
                                    </td>
                                    <td align="left">
                                        <label for="maintenance_mode_level_off">
                                            <strong><?php _e('Message when "Show courses on specified dates" setting enabled', 'cs_ld_addon'); ?></strong>
                                        </label>
                                        <p class="description" style="font-weight: normal;">
                                            <?php _e('This message will be shown on frontend when user topic page and that course has been scheduled for a later date with the first setting option being selected in the general settings. You can use <strong>[cs_scheduled_dates]</strong> to get the scheduled dates', 'cs_ld_addon'); ?>
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <?php wp_editor( esc_textarea( $hide_topic_message ), "hide_topic_message", $settings = array() ); ?>
                                    </td>
                                    <td align="left">
                                        <label for="maintenance_mode_level_off">
                                            <strong><?php _e('Message when "Show courses except the specified dates" setting enabled', 'cs_ld_addon'); ?></strong>
                                        </label>
                                        <p class="description" style="font-weight: normal;">
                                            <?php _e('This message will be shown on frontend when user visits topic page and that course has been scheduled for dates except the ones set on calendar, with the second setting option being selected in the general settings. You can use <strong>[cs_scheduled_dates]</strong> to get the scheduled dates', 'cs_ld_addon'); ?>
                                        </p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <p>
                    <input type="submit" class="button-primary cs-ld-addon-btn" value="Update Settings">
                </p>
                <?php wp_nonce_field( 'csld_settings', 'csld_settings_field' ); ?>
            </form>
        </div>
    </div>
</body>