<?php
namespace CSLD;
/**
 * Abort if this file is accessed directly.
 */
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}
?>
<body>
    <div id='wrap'>
        <div class="branding">
            <img class="branding-logo" src="<?php echo ASSETS_URL . "wooninjas.png"; ?>" alt="">
            <span class="branding-text">For any further support or assistance for the plugin kindly reach out to us at <a href="http://wooninjas.com/contact-us" target="_blank">Wooninjas</a><br> If you like our plugin kindly take some time to leave a review and a rating for us <br><a href="https://wordpress.org/plugins/course-scheduler-for-learndash/" target="_blank" class="csld_support">Review Course Scheduler for LearnDash</a></span>
        </div>
        <div id='external-events'>
            <h4><?php _e( "Select Courses", "cs_ld_addon" ) ?></h4>
            <?php
            foreach( $courses as $course ) {
                $data = get_post_meta( $course->ID, 'course_schedule', true ); ?>
                <div class='fc-event' data-course-id="<?php echo $course->ID; ?>">
                    <?php echo $course->post_title; ?>
                </div>
            <?php } ?>
        </div>
        <div id='calendar'></div>
        <div style='clear:both'></div>
    </div>
</body>