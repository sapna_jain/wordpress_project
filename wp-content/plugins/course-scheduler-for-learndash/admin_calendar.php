<?php
namespace CSLD;
/**
 * Plugin Name: Course Scheduler for LearnDash
 * Plugin URI: http://wooninjas.com/
 * Description: Enables scheduling of LearnDash Courses on Calendar
 * Version: 1.3
 * Author: Wooninjas
 * Author URI: http://wooninjas.com/
 * Text Domain: cs_ld_addon
 * License: GNU General Public License v2.0
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 */

/**
 * Abort if this file is accessed directly.
 */
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Check if LD is enabled
 */
function require_dependency( ) {
    if ( !is_plugin_active( 'sfwd-lms/sfwd_lms.php' ) ) {
        deactivate_plugins( plugin_basename( __FILE__ ) );
        $class = 'error';
        $message = __( 'Course Scheduler for LearnDash requires LearnDash plugin to be activated.', 'cs_ld_addon' );
        printf( '<div class="%s"> <p>%s</p></div>', $class, $message );
    }
}
add_action( 'admin_notices', __NAMESPACE__ . '\\require_dependency' );

/**
 * LearnDash Version Constant
 */
define( 'CSLD\CS_LD_VERSION', '1.3' );
define( 'CSLD\CS_LD_TEXT_DOMAIN', 'cs_ld_addon' );
define( 'CSLD\CS_LD_PLUGIN_NAME', plugin_basename( __FILE__ ) );

/**
 * Learndash Directory Constants
 */
define( 'CSLD\DIR', plugin_dir_path(__FILE__));
define( 'CSLD\DIR_FILE', DIR.basename(__FILE__));
define( 'CSLD\INCLUDES_DIR', trailingslashit(DIR.'includes'));

/**
 * Learndash URL Contants
 */
define( 'CSLD\CS_LD_PLUGIN_URL', plugins_url( '', __FILE__ ) );
define( 'CSLD\ASSETS_URL', trailingslashit(CS_LD_PLUGIN_URL.DIRECTORY_SEPARATOR.'assets'));

// Autoload classes for the plugin
spl_autoload_register(__NAMESPACE__ .'\Main::autoloader');

/**
 * Class Main for plugin initiation
 *
 * @since 1.0
 */
final class Main {
    public static $version = '1.3';

    // Main instance
    protected static $_instance = null;

    protected function __construct(  ) {
        register_activation_hook( __FILE__, array( __CLASS__, 'activation' ) );
        register_uninstall_hook( __FILE__, array( __CLASS__, 'uninstall' ) );

        // Upgrade
        add_action( 'plugins_loaded', array( $this, 'upgrade' ) );

        // Adding settings tab
        add_filter( 'plugin_action_links_'.plugin_basename( DIR_FILE ), function( $links ) {
            return array_merge( $links, array(
                sprintf(
                    '<a href="%s">Settings</a>',
                    admin_url( 'admin.php?page=calendar_course_settings' )
                ),
            ) );
        } );
        new CS_LD_HELPER();
    }

    public static function autoloader($class) {
        $class = str_replace( __NAMESPACE__ ."\\" , "" , $class );
        if( file_exists( INCLUDES_DIR  . $class . '.php' ) ) {
            include INCLUDES_DIR  . $class . '.php';
        }
    }

    /**
     * @return $this
     */
    public static function instance() {
        if ( is_null( self::$_instance ) ) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    /**
     * Activation function hook
     *
     * @return void
     */
    public static function activation() {
        if (!current_user_can('activate_plugins'))
            return;

        update_option('csld_version', self::$version);
        update_option( 'course_scheduler_ld_addon_setting', 1 );
        update_option( 'cs_ld_addon_show_course_message', __("The course will be available on below date/s: [cs_scheduled_dates]", "csld") );
        update_option( 'cs_ld_addon_hide_course_message', __("The course will not be available on below date/s: [cs_scheduled_dates]", "csld") );
        update_option( 'cs_ld_addon_show_lesson_message', __("The lesson will be available on below date/s: [cs_scheduled_dates]", "csld") );
        update_option( 'cs_ld_addon_hide_lesson_message', __("The lesson will not be available on below date/s: [cs_scheduled_dates]", "csld") );
        update_option( 'cs_ld_addon_show_quiz_message', __("The quiz will be available on below date/s: [cs_scheduled_dates]", "csld") );
        update_option( 'cs_ld_addon_hide_quiz_message', __("The quiz will not be available on below date/s: [cs_scheduled_dates]", "csld") );
        update_option( 'cs_ld_addon_show_topic_message', __("The topic will be available on below date/s: [cs_scheduled_dates]", "csld") );
        update_option( 'cs_ld_addon_hide_topic_message', __("The topic will not be available on below date/s: [cs_scheduled_dates]", "csld") );
    }

    /**
     * Deactivation function hook
     *
     * @return void
     */
    public static function uninstall() {
        delete_option( 'csld_version' );
        delete_option( 'course_scheduler_ld_addon_setting' );
        delete_option( 'cs_ld_addon_show_course_message' );
        delete_option( 'cs_ld_addon_hide_course_message' );
        delete_option( 'cs_ld_addon_show_lesson_message' );
        delete_option( 'cs_ld_addon_hide_lesson_message' );
        delete_option( 'cs_ld_addon_show_topic_message' );
        delete_option( 'cs_ld_addon_hide_topic_message' );
        delete_option( 'cs_ld_addon_show_quiz_message' );
        delete_option( 'cs_ld_addon_hide_quiz_message' );
    }

    /**
     * Upgrade function hook
     */
    public static function upgrade() {
        if ( get_option( 'csld_version' ) != self::$version ) {
            
        }
    }
}

/**
 * Main instance
 *
 * @return Main
 */
function CSLD() {
    return Main::instance();
}

CSLD();